import random
import lpt
import greedy
import random_tasks
START_POP=5

class task:
    def __init__(self, task_id, proc_id, init_time, end_time):
        self.init_time = init_time
        self.task_id = task_id
        self.proc_id = proc_id
        self.end_time = end_time
class genotype_c:
    def __init__(self,genotype,rank:float):
        self.genotype=genotype
        self.rank=rank

def crossover(genotype_1, genotype_2):
    offspring_1=[]
    offspring_2=[]
    if(random.randint(0,1)==0):
        for i in range(len(genotype_1)):
             if(random.randint(0,1)==0):
                offspring_1.append(genotype_1[i])
                offspring_2.append(genotype_2[i])
             else:
                offspring_1.append(genotype_2[i])
                offspring_2.append(genotype_1[i])

    else:
        piv=random.randint(1,len(genotype_1)-1)
        offspring_1=genotype_1[:piv]+genotype_2[piv:]
        offspring_2=genotype_2[:piv]+genotype_1[piv:]

    return offspring_1, offspring_2


def encode(task_table):
    genotype = []
    for task in task_table:
        genotype.append(task.proc_id)
    return genotype



def fitness(processlist,proc_number,genotype):
    processor_list=[]
    processor_times=[]
    for i in range(proc_number):
        processor_list.append([])
    for i in range(len(processlist)):
        processor_list[genotype[i]].append(processlist[i])
    for processor in processor_list:
        processor_times.append(sum(processor))
    return[max(processor_times),processor_times,processor_list]



def loop(process_list,processors_number,generations):
    process_list_1=process_list.copy()
    process_list_2 = process_list.copy()
    parent_1=greedy.manage_tasks_greedy(processors_number,process_list_1)[1]
    par=parent_1
    parent_2=greedy.manage_tasks_greedy(processors_number,process_list_2)[1]
    genotype_1=encode(parent_1)
    genotype_2=encode(parent_2)
    gen_greedy=genotype_1
    genotypes=[]
    fit=[]
    genotypes.append(lpt.LPT())
    genotypes.append(genotype_1)
    genotypes.append(genotype_2)
    for k in range(START_POP):
        process_list_1=process_list.copy()
        parent=random_tasks.manage_tasks_random(processors_number,process_list_1)[1]
        genotype=encode(parent)
        genotypes.append(genotype)
    for i in range(generations):
        genotype_1,genotype_2=crossover(genotypes[random.randint(0,len(genotypes)-1)],genotypes[random.randint(0,len(genotypes)-1)])
        if(random.randint(0,100)>70):
                x=random.randint(1,len(genotype_1)-2)
                tmp=genotype_1[x]
                genotype_1[x]=genotype_1[x+1]
                genotype_1[x+1]=tmp
        
        if(random.randint(0,100)>70):
                x=random.randint(1,len(genotype_2)-2)
                tmp=genotype_1[x]
                genotype_2[x]=genotype_2[x+1]
                genotype_2[x+1]=tmp
        if fitness(process_list, processors_number, genotype_1)[0]<fitness(process_list, processors_number, gen_greedy)[0]:

            genotypes.append(genotype_1)
        if fitness(process_list, processors_number, genotype_2)[0] < fitness(process_list, processors_number, gen_greedy)[0]:
            genotypes.append(genotype_2)
        if (fitness(process_list,processors_number,genotype_1)[0] <fitness(process_list, processors_number, gen_greedy)[0]):
           
            genotypes.append(genotype_1)

            fit.append(fitness(process_list,processors_number,genotype_1)[0])
            print(min(fit))
#TODO: LPT do puli
        if (fitness(process_list,processors_number,genotype_2)[0] <fitness(process_list, processors_number, gen_greedy)[0] ):
            genotypes.append(genotype_2)
            fit.append(fitness(process_list,processors_number,genotype_2)[0])
        if (random.randint(0,10)>8 and len(genotypes)>8):
            genotypes.pop(0)
        print(genotype_1)
        print(genotype_2)
    fitness_gen=[]
    for gen in genotypes:
        fitness_gen.append(fitness(process_list,processors_number,gen))
    index_min = min(range(len(fitness_gen)), key=fitness_gen.__getitem__)
    print(genotypes[index_min])
    print("minimalny dla genetycznego: ",min(fit))
    print('Maksymalny czas: %d'% fitness(process_list,processors_number,genotypes[index_min])[0])
    for element in range(len(fitness(process_list,processors_number,genotypes[index_min])[1])):
        print("Czas dla procesora %d:"%element,(fitness(process_list,processors_number,genotypes[index_min])[1][element]))
    for element in range(len(fitness(process_list,processors_number,genotypes[index_min])[2])):
        print("Procesy przydzielone do procesora %d:"%element,(fitness(process_list,processors_number,genotypes[index_min])[2][element]))
    return genotypes
