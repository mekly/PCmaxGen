import greedy
import genetic
import random_tasks

from copy import deepcopy


def main():
    # greedy.gen_instances(5, 100, "file.txt", 5, 1000)
    data_tuple_first = greedy.get_data("file.txt")
    data_tuple = deepcopy(data_tuple_first)
    table = greedy.manage_tasks_greedy(data_tuple[0], data_tuple[2])
    for element in table[0]:
        ind = table[0].index(element)
        print('Procesor:%d  %d' % (ind, element))

    list_tasks_greedy = table[1]
    data_tuple = deepcopy(data_tuple_first)
    table2 = random_tasks.manage_tasks_random(data_tuple[0], data_tuple[2])
    list_tasks_random = table2[1]
    for task2 in list_tasks_random:
        print(task2.task_id, task2.end_time)
    print("Greedy:")
    tab_temp = []
    for task in list_tasks_greedy:
        print(task.task_id, task.end_time)
        tab_temp.append(task.end_time)
    data_tuple = deepcopy(data_tuple_first)
    genetic.loop(data_tuple[2], data_tuple[0], 10000)
    print('czas dla zachłannego: %d' % max(tab_temp))


if __name__ == "__main__":
    main()
