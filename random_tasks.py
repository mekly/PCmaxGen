import genetic
import greedy
import random
def manage_tasks_random(n_processors, data):
    processors = []
    proc_queue=[]
    for n in range(n_processors):
        processors.append(0)
    for n in range(n_processors):
        proc_queue.append(0)

    min(processors)
    task_index=0
    task_list=[]
    while data:
        index_random =random.randint(0,len(processors)-1)
        temp=data.pop(0)
        task_index+=1
        start_time=(processors[index_random])
        end_time=(processors[index_random])+temp
        task_list.append(genetic.task(task_index,index_random,start_time,end_time))
        processors[index_random] += temp
        proc_queue.append(temp)
    return [processors,task_list]