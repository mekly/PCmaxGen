import random_tasks
import random
import genetic


def gen_instances(processors: int, tasks: int, file_path: str, limit_down: int, limit_up: int):
    file = open(file_path, 'w')
    file.write(str(processors) + "\n" + str(tasks) + "\n")
    for i in range(tasks):
        random_number = random.randint(limit_down, limit_up)
        file.write(str(random_number) + "\n")


def get_data(file_path: str):
    file = open(file_path, 'r')
    processors = int(file.readline())
    tasks = int(file.readline())
    print(tasks)
    data = []
    for i in range(tasks):
        tmp = int(file.readline())
        data.append(tmp)
    return [processors, tasks, data]


def manage_tasks_greedy(n_processors, data):
    processors = []
    proc_queue=[]
    for n in range(n_processors):
        processors.append(0)
    for n in range(n_processors):
        proc_queue.append(0)
    min(processors)
    task_index=0
    task_list=[]
    while data:
        index_min = min(range(len(processors)), key=processors.__getitem__)
        temp=data.pop(0)
        task_index+=1
        start_time=(processors[index_min])
        end_time=(processors[index_min])+temp
        task_list.append(genetic.task(task_index,index_min,start_time,end_time))
        processors[index_min] += temp
        proc_queue.append(temp)

    return [processors,task_list]
